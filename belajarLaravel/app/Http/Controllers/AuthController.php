<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view('page.form');
    }
    public function send(Request $request)
    {
        $namaDepan = $request['first'];
        $namaBelakang = $request['last'];

        return view('page.home', ['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);
    }
}
