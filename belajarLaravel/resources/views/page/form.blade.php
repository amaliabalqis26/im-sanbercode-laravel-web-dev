@extends('layouts.master')
@section('title')
Halaman Biodata
@endsection

@section('sub-title')
biodata
@endsection

@section('content')
<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>
<form action="/welcome" method="post">
@csrf
    <label>First name:</label><br><br>
    <input type="text" name="first"><br><br>
    <label>Last name:</label><br><br>
    <input type="text" name="last"><br><br>
    <label>Gender</label><br><br>
    <input type="radio">Male<br>
    <input type="radio">Female<br>
    <input type="radio">Other<br><br>

    <label>Nationality:</label><br><br>
    <select name="nationality">
        <option value="Indonesian">Indonesian</option>
        <option value="Japanese">Japanese</option>
        <option value="Malaysian">Malaysian</option>
        <option value="Korean">Korean</option>
    </select><br><br>

    <label>Language Spoken:</label><br><br>
    <input type="checkbox" name="Language">Bahasa Indonesia <br>
    <input type="checkbox" name="Language">English <br>
    <input type="checkbox" name="Language">Other <br><br>

    <label>Bio:</label><br><br>
    <textarea name="bio"cols="30" rows="10"></textarea><br>
    

    <input type="Submit" value="Sign Up">
    
</form>
@endsection
